#!/bin/bash
#===================================================================================
# kvm_create_internal_snapshot.sh - Cria snapshots internos de máquinas virtuais KVM
#
# Autor: Ian Nascimento
#===================================================================================

# Definição das variáveis de controle
ARQUIVO_LOG="./kvm-create-snapshot.log"
ARQUIVO_LOCK="./kvm-create-snapshot.lock"

MENSAGEM_DE_AJUDA="Uso: $0 [-h, --help | -vm, --virtual-machine]
-h, --help			    Exibe essa mensagem de ajuda
-vm, --virtual-machine <nome_da_vm> Nome da máquina virtual para a criação do snapshot
"

# Função para gerar logs em arquivo
gerar_log(){
  printf "$(date "+%b %d %H:%M:%S") $1\n" >> $ARQUIVO_LOG
}

# Verifica se o arquivo de lock existe.
# Caso não exista, o arquivo é gerado e uma trap é definida para ser acionada no fim da execução do script.
if { set -C; 2>/dev/null >${ARQUIVO_LOCK}; }; then
  trap "rm -f $ARQUIVO_LOCK" EXIT
else
  gerar_log "O programa já está em execução... Finalizando"
  exit 1
fi

case $1 in
    -h | --help)
      printf "$MENSAGEM_DE_AJUDA"
      exit 0
    ;;

    -vm | --virtual-machine)
      maquina_virtual=$2
      nome_snapshot=$(date '+%Y%m%d')
      virsh snapshot-create-as --domain ${maquina_virtual} --name ${nome_snapshot}
      if [ $? -eq 0 ]; then
        printf "Snapshot ${nome_snapshot} da máquina virtual ${maquina_virtual} criado"
        gerar_log "Snapshot ${nome_snapshot} da máquina virtual ${maquina_virtual} criado"
        exit 0
      else
        saida_de_erro=$(virsh snapshot-create-as --domain ${maquina_virtual} --name ${nome_snapshot} 2>&1 1>/dev/null)
        printf "Falha ao criar snapshot. Verifique a saída de log para mais detalhes."
        gerar_log "Falha ao criar snapshot: ${saida_de_erro}"
        exit 1
      fi
    ;;

    *)
      if [ -z $1 ]; then
        printf "Nenhum parâmetro foi informado.\n$MENSAGEM_DE_AJUDA"
      else
        printf "Opção inválida: $1"
      fi
      exit 1
    ;;
esac

exit 0
